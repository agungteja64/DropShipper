<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Commission extends Model
{
    protected $table = 'commissions';

    protected $fillable = [
        'status'
    ];

    public function orders()
    {
    	return $this->HasMany(Order::class);
    }
}
