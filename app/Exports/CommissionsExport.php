<?php

namespace App\Exports;

use App\Commission;
use Maatwebsite\Excel\Concerns\FromCollection;
//use Maatwebsite\Excel\Concerns\FromQuery;

use Illuminate\Support\Facades\DB;

class CommissionsExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */


    public function collection()
    {
//    	$commissions = Commission::with(['orders.user.profile'])->where('status', 0)->get()->all();
    	$aa = DB::table('commissions')
            ->join('order_commission', 'commissions.id', '=', 'order_commission.commission_id')
            ->join('users', 'order_commission.user_id', '=', 'users.id')
            ->join('profiles', 'users.id', '=', 'profiles.user_id')
            ->select(['commissions.id', 'profiles.name as nama', 'commissions.created_at as tgl', 'commissions.commission as komisi', 'profiles.bank_account as rekening', 'profiles.bank_type as bank'])
            ->where('commissions.status', 0)
            ->orderBy('profiles.name')
            ->get();
    	/*
    	$data_excel = array();
    	foreach ($commissions as $commission) {
    		array_push($data_excel, array([$commission->orders[0]->user->profile->name, $commission->created_at, $commission->commission, $commission->orders[0]->user->profile->bank_account, $commission->orders[0]->user->profile->bank_type]));
    		
    		$data_excel[$i]->name = $commission->orders[0]->user->profile->name;
    		$data_excel[$i]->tgl = $commission->created_at;
    		$data_excel[$i]->jml = $commission->commission;
    		$data_excel[$i]->no_rek = $commission->orders[0]->user->profile->bank_account;
    		$data_excel[$i]->bank = $commission->orders[0]->user->profile->bank_type;
    		
    	}
	*/
    	//$a = collect($data_excel);

        return $aa;
    }
    /*
    public function query()
    {
    	//return Commission::with(['orders.user.profile'])->where('status', 0);
    	return $aa = DB::table('commissions')
            ->join('order_commission', 'commissions.id', '=', 'order_commission.commission_id')
            ->join('users', 'order_commission.user_id', '=', 'users.id')
            ->join('profiles', 'users.id', '=', 'profiles.user_id')
            ->select(['profiles.name as nama', 'commissions.created_at as tgl', 'commissions.commission as komisi', 'profiles.bank_account as rekening', 'profiles.bank_type as bank'])
            ->get();
    }
    */
}
