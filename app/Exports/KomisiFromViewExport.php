<?php

namespace App\Exports;

use App\Commission;
//use Maatwebsite\Excel\Concerns\FromCollection;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class KomisiFromViewExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function View(): View
    {
    	$commissions = Commission::with(['orders.user.profile'])->where('status', 0)->get();
    	return view('spreadsheet.komisi', ['commissions' => $commissions]);
    }
}
