<?php

namespace App\Exports;

use App\Order;
use Maatwebsite\Excel\Concerns\FromCollection;

class OrdersExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */

    private $commission;

    function __construct($commission)
    {
    	$this->commission = $commission;
    }

    public function collection()
    {
        //return Order::all();
        return Order::where('commission_id', $this->commission->id)->get();
    }
}
