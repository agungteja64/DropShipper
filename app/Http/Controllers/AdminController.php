<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Video;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Exception;

class AdminController extends Controller
{

    public function list_user()
    {
    	$users = User::with(['store', 'profile'])->get();
    	return view('list_user', ['users' => $users]);
    }

    public function activate_user(Request $request)
    {
    	if (Auth::user()->role_id != 1) {
    		return view('not_admin');
    	}
    	try {
    		DB::beginTransaction();

    		$user = User::find($request->user_id);
    		$user->status = 2;
    		$user->save();

    		DB::commit();
    	} catch (Exception $e) {
    		DB::rollBack();
    		return "gagal mengubah status user";
    	}

    	return redirect('admin/users');
    }

    public function list_tutorial()
    {
    	$videos = Video::all();

    	return view('list_tutorials', ['videos' => $videos]);
    }

    public function show_add_tutorials()
    {
    	return view('add_tutorials');
    }

    public function add_tutorials(Request $request)
    {
    	try {
    		DB::beginTransaction();

    		$video = new Video;
    		$video->title = $request->title;
    		$video->link = $request->link;
    		$video->save();

    		DB::commit();
    	} catch (Exception $e) {
    		DB::rollBack();
    		return "Gagal, Exception: {$e}";
    	}

    	return redirect()->route('admin-tutorials');
    }

}
