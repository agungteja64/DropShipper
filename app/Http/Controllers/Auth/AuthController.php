<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function userNotActive()
    {
    	$status = Auth::user()->status;
    	Auth::logout();

    	if ($status == 0) {
	    	return view('activate_your_acc_email');
    	}elseif($status == 1) {
    		Auth::logout();
	    	return view('activate_your_acc_billing');
    	}

    	return redirect()->route('store-detail');
    }
}
