<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Exception;

use App\Woocommerce;
use App\Order;
use App\Commission;

use App\Exports\ordersExport;
use App\Exports\CommissionsExport;
use App\Exports\KomisiFromViewExport;

//use Maatwebsite\Excel\Excel;
use Maatwebsite\Excel\Facades\Excel;

use App\Repository\UserCommissionImpl;
use App\Repository\ProductVendorRepoImpl;

class CommissionController extends Controller
{
    private $woocommerce;
	private $user_commission;
	private $product_vendor;

    function __construct()
    {
    	$wc = new Woocommerce;
        $this->woocommerce = $wc->woocommerce;
		$this->product_vendor = new ProductVendorRepoImpl(
			$_ENV["VENDOR_URL"],
			$_ENV['VENDOR_API'],
			$_ENV["WOO_CK"],
			$_ENV["WOO_CS"]
		);
    }

    private function assignOrderFromWC($orders)
    {
    	foreach ($orders as $order) {
    		$orderFromWC = $this->woocommerce->get('orders/'.$order->order_id);
    		//dd($orderFromWC);
    		$order->statusOrder = $orderFromWC->status;
    		$order->itemLists = $orderFromWC->line_items;
    		$order->total = $orderFromWC->total;
    	}

    	return $orders;
    }

	public function get_comission() 
	{
		$this->user_commission = new UserCommissionImpl(
			Auth::user()->id, $this->product_vendor
		);

		return view('komisi_reseller', [ 
			'total_commission' => $this->user_commission->total_commission(),
			'list_order' => $this->user_commission->get_list_comm_order(),
            'percent_commission' => $this->user_commission->percent_commission,
		]);
	}


/*
    public function show_all_orders()
    {
    	//dd($this->woocommerce->get('orders'));
    	$orders = Order::with('user')->get();
    	$orders = $this->assignOrderFromWC($orders);
    	return view('list_commissions', ['orders' => $orders]);
    }
*/
    public function showCommissions()
    {
        $commissions = Commission::with(['orders', 'orders.user.profile', 'orders.user.store'])->get();
        return view('list_commissions', ['commissions' => $commissions]);
    }

/*
    public function change_status_commissions(Request $request)
	{
		try {
			DB::beginTransaction();
			$order = Order::where(['user_id' => $request->user_id, 'order_id' => $request->order_id])->first();
			$order->commission_status = 1;
			$order->save();
			DB::commit();
		} catch (Exception $e) {
			DB::rollBack();
			return "Gagal, Exception: {$e}";
		}
		return redirect()->route('admin-commissions');
	}
*/

    public function change_status_commissions(Request $request)
    {
        try {
            DB::beginTransaction();
            $commission = Commission::find($request->id);
            $commission->status = 1;
            $commission->save();
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            session()->flash('error_message', 'Status komisi gagal diubah '.$e->getMessage());
            return back();
        }
        session()->flash('success_message', 'Status komisi berhasil diubah');
        return back();
    }

/*
    public function getCommissionStatusZero()
    {
        $orders = Order::with('user')->where(['commission_id' => NULL, 'user_id' => Auth::user()->id])->get();
        $orders = $this->assignOrderFromWC($orders);

        //dd($orders);
        return view('list_commissions', ['orders' => $orders]);
    }
*/

    /**
     * createCommission create a commission for a given user
     * based on their commission value right when the user request.
     * (The request may happen in periodic time like weekly, or monthly).
     */
    public function createCommission(Request $request)
    {
        // get the user commission value
        $this->user_commission = new UserCommissionImpl(
			Auth::user()->id, $this->product_vendor
        );
        
        $total_comm = $this->user_commission->total_commission();
        
        // fail and return if $total_comm is 0
        if($total_comm == 0) {
            session()->flash('error_message', 'Komisi kosong');
            return view('komisi_reseller', [ 
                'total_commission' => $this->user_commission->total_commission(),
                'list_order' => $this->user_commission->get_list_comm_order(),
                'percent_commission' => $this->user_commission->percent_commission,
            ]);
        }
        
        // crete the user commission
        try {
            DB::beginTransaction();

            $commission = new Commission;
            $commission->status = 0;
            $commission->commission = $total_comm;
            $commission->save();

            // change the commission status of current orders to 0
            foreach ($request->orders as $order) {
                $orderdb = Order::where('order_id', $order)->first();
                $orderdb->commission_id = $commission->id;
                $orderdb->save();
            }

            //Excel::download(new ordersExport($commission), 'orders.xlsx');

            //Excel::store(new ordersExport($commission), 'public/invoices.xlsx');

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();

            session()->flash('error_message', 'Komisi gagal dibuat '.$e->getMessage());

            return back();
        }

        session()->flash('success_message', 'Komisi berhasil dibuat');
        //session()->flash('download.in.the.next.request', url('').'/storage/invoices.xlsx');
        
        return back();
    }

    public function downloadRecapCommissions()
    {
        return Excel::download(new KomisiFromViewExport, 'komisi.xlsx');;
    }

}
