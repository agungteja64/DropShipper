<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Promotion;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Exception;
use Illuminate\Support\Facades\Storage;

class PromotionController extends Controller
{
    public function list_promotion()
    {
    	$promotions = Promotion::all();

    	return view('list_promotions', ['promotions' => $promotions]);
    }

    public function show_add_promotions()
    {
    	return view('add_promotions');
    }

    public function add_promotions(Request $request)
    {
    	try {
    		DB::beginTransaction();

    		$promotion = new Promotion;
    		$path = $request->path->storeAs('/promotion', uniqid().time().'.'.$request->path->getClientOriginalExtension(), 'public');
    		$promotion->title = $request->title;
    		$promotion->path = $path;
    		if ($request->type == 0) {
    			$promotion->type = 'Image';
    		}
    		$promotion->status = 0;
    		$promotion->save();

    		DB::commit();
    	} catch (Exception $e) {
    		DB::rollBack();
    		return "Gagal". $e;
    	}

    	return redirect()->route('admin-promotions');
    }

	public function delete_promotion($id) {
		$promotion = Promotion::where(['id' => $id])->first();

		// Delete actual promotion
		Storage::delete('/public/'.$promotion->path);

		// Delete DB record
		$promotion->destroy($id);

		return view('list_promotions', ['promotions' => Promotion::all()]);
	}

    public function download_promotion($path)
    {
    	$name = Promotion::where('path', 'promotion/'.$path)->first();
    	return Storage::download('public/promotion/'.$path, $name->title.'.'.substr($path, strpos($path, '.') +1));
    }
}
