<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Store;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Exception;
use Automattic\WooCommerce\Client;
use App\Vendor;
use App\Promotion;
use App\Video;
use App\Order;
use App\Woocommerce;
use App\Repository\UserCommissionImpl;
use App\Repository\ProductVendorRepoImpl;

/**
 * TODO: 
 * - [ ] Handle view for empty value (ie. when error/exception happend)
 */
class StoreController extends Controller
{
	protected $woocommerce;
	private $user_commission;
	private $product_vendor;
	
	public function __construct()
    {   
        $wc = new Woocommerce;
		$this->woocommerce = $wc->woocommerce;
		$this->product_vendor = new ProductVendorRepoImpl(
			$_ENV["VENDOR_URL"],
			$_ENV["VENDOR_API"],
			$_ENV["WOO_CK"],
			$_ENV["WOO_CS"]
		);
    }

    public function detail()
    {
    	$store = Store::find(Auth::user()->store->id);
    	return view('StoreDetail', ['store' => $store]);
    }

    public function update(Request $request)
    {
    	try {
    		DB::beginTransaction();

    		$store = Store::find(Auth::user()->store->id);
            $store->sosmed_fb = $request->sosmed_fb;
            $store->sosmed_instagram = $request->sosmed_instagram;
            $store->sosmed_line = $request->sosmed_line;
            $store->sosmed_wa = $request->sosmed_wa;
            if ($request->logo != NULL) {
        		$path = $request->logo->storeAs('', uniqid().time().'.'.$request->logo->getClientOriginalExtension(), 'public');
                $store->logo = $path;
            }
            $store->save();

    		DB::commit();
    	} catch (Exception $e) {
    		DB::rollBack();
    		return redirect()->route('store-detail');
    	}
    	return redirect()->route('store-detail');
    }


    public function vendor_list()
    {
    	$subscribed_product = Vendor::where('store_id', Auth::user()->store->id)->get(['wc_cat_id'])->toArray();
    	$subscribed_product_id = array();
    	foreach ($subscribed_product as $value) {
    		array_push($subscribed_product_id, $value['wc_cat_id']);
    	}

    	$product_categories = $this->product_vendor->get_products();

		// if product is empty, return empty page
		if(@count($product_categories) == 0) {
			return view('empty/products');
		}

    	$subscribed = array();

    	foreach ($product_categories as $key => $value) {
    		if (in_array($value->id, $subscribed_product_id)) {
    			array_push($subscribed, $product_categories[$key]);
    			unset($product_categories[$key]);
    		}
    	}
		
    	return view('products', ['subscribed' => $subscribed, 'unsubscribed' => $product_categories]);
    }


	public function vendor_by_category($cat_id) {
		$subscribed_product = Vendor::where('store_id', Auth::user()->store->id)->get(['wc_cat_id'])->toArray();
    	$subscribed_product_id = array();
    	foreach ($subscribed_product as $value) {
    		array_push($subscribed_product_id, $value['wc_cat_id']);
    	}

		$categories = $this->woocommerce->get('products/categories');
		$product_categories = $this->product_vendor->get_product_from_category($cat_id);

		$subscribed = array();
		
    	foreach ($product_categories as $key => $value) {
    		if (in_array($value->id, $subscribed_product_id)) {
    			array_push($subscribed, $product_categories[$key]);
    			unset($product_categories[$key]);
    		}
		}

		$unsubscribed = array_chunk($product_categories, 4);
		$subscribed = array_chunk($subscribed, 4);

    	return view('products', [
			'subscribed' => $subscribed, 
			'unsubscribed' => $unsubscribed,
			'categories' => $categories,
		]);
	}

    public function vendor_update(Request $request)
    {
    	try {
    		DB::beginTransaction();

    		Vendor::where('store_id', Auth::user()->store->id)->delete();

    		foreach ($request->category as $category) {
    			$cat = new Vendor;
    			$cat->store_id = Auth::user()->store->id;
    			$cat->wc_cat_id = $category;
    			$cat->save();
    		}

    		DB::commit();
    	} catch (Exception $e) {
    		DB::rollBack();

    		return redirect()->route('products-list');
    	}
    	return redirect()->route('products-list');
    }

    public function order_list()
    {
    	$order_list = Order::where('user_id', Auth::user()->id)->get()->toArray();

    	echo "<pre>";
    	var_dump($order_list);
    	echo "</pre>";
    	die();
	}
	
	public function get_comission() 
	{
		$this->user_commission = new UserCommissionImpl(
			Auth::user()->id, $this->product_vendor
		);

		return view('komisi_reseller', [ 
			'total_commission' => $this->user_commission->total_commission(),
			'list_order' => $this->user_commission->get_list_comm_order(),
            'percent_commission' => $this->user_commission->percent_commission,
		]);
	}

	public function history_comsission()
	{
		$this->user_commission = new UserCommissionImpl(
			Auth::user()->id, $this->product_vendor
		);
		
		echo "<h1 style='text-align: center; margin-top: 2em;'><em>On Progress<em></h1>";
		return;

		// $this->user_commission->history_comsission();
	}

	public function show_marketing_tuts() 
	{
		// get the list of imers tutorial video
		$videos = Video::all();

    	return view('user_list_tutorials', ['videos' => $videos]);
	}

	public function show_marketing_content()
	{
		$promotions = Promotion::all();

		return view('user_list_promotion', [
			'promotions' => $promotions,
		]);
	}
}
