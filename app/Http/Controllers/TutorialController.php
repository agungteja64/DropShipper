<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Video;

use Illuminate\Support\Facades\DB;
use Exception;

class TutorialController extends Controller
{
    public function list_tutorial()
    {
    	$videos = Video::all();

    	return view('list_tutorials', ['videos' => $videos]);
    }

    public function show_add_tutorials()
    {
    	return view('add_tutorials');
    }

    public function add_tutorials(Request $request)
    {
    	try {
    		DB::beginTransaction();

    		$video = new Video;
    		$video->title = $request->title;
    		$video->link = $request->link;
    		$video->save();

    		DB::commit();
    	} catch (Exception $e) {
    		DB::rollBack();
    		return "Gagal";
    	}

    	return redirect()->route('admin-tutorials');
    }
}
