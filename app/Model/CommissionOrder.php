<?php
namespace App\Model;

class CommissionOrder {
    private $order_id;
    private $commission;

    public function __construct($order_id, $commission) {
        $this->order_id = $order_id;
        $this->commission = $commission;
    }

    public function get_order_id() {
        return $this->order_id;
    }

    public function get_commission() {
        return $this->commission;
    }
}