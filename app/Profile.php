<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{

	protected $table = 'profiles';

    protected $fillable = [
        'name', 'contact', 'address', 'bank_account', 'user_id', 'bank_type'
    ];

    public function user()
    {
    	return $this->belongsTo(User::class);
    }

}
