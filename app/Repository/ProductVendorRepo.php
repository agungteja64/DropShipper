<?php

namespace App\Repository;

interface ProductVendorRepo {
    public function get_product_from_category($category_id);
    public function make_order($orders);
    public function get_order($order_id);
    public function get_product($id);
    public function get_products();
    public function get_products_by_param_ids(string $param, array $ids);
}