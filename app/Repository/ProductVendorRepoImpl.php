<?php
namespace App\Repository;

use Automattic\WooCommerce\Client;

use GuzzleHttp\Client as Guzzle;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Handler\CurlHandler;
use GuzzleHttp\Subscriber\Oauth\Oauth1;

use Exception;

use App\Repository\ProductVendorRepo;

class ProductVendorRepoImpl implements ProductVendorRepo {
    private $cs;
    private $ck;
    private $woocomm;
    private $base_url;
    private $guzzle;
    private $base_api;

    public function __construct($base_url, $base_api, $ck, $cs) {
        $this->base_url = $base_url;  
        $this->base_api = $base_api;
        $this->ck = $ck;
        $this->cs = $cs;

        // setup guzzle to use Basic Auth
        $this->guzzle = new Guzzle([
            'base_uri' => $this->base_url.$this->base_api,
            'auth' => [$ck, $cs]
        ]);

        $this->woocomm = new Client(
            $base_url, 
            $ck, 
            $cs,
            [
                'wp_api' => true,
                'version' => 'wc/v2',
                'timeout' => 1000,
                'query_string_auth' => true,
            ]
        );

    }
    
    public function make_order($orders) {
        try {
            $res = $this->guzzle->request("POST", "orders", ["json" => $orders]);
            return json_decode($res->getBody());
        } catch(Exception $e) {
            // Logging $e
            Log::error($e);
            
            // return empty object
            return new stdClass();
        }
    }

    public function get_order($order_id) {
        try {
            $res = $this->guzzle->get("orders/{$order_id}");
            return json_decode($res->getBody());
        } catch(Exception $e) {
            // Logging $e
            Log::error($e);
            
            // return empty object
            return new stdClass();
        }
    }

    public function get_product($id) {
        try {
            $res = $this->guzzle->get("products/{$id}");
            return json_decode($res->getBody());
        } catch(Exception $e) {
            // Logging $e
            Log::error($e);
            
            // return empty object
            return new stdClass();
        }
    }

    public function get_products() {
        try {
            $res = $this->guzzle->get("products");
            // dd($res);
            return json_decode($res->getBody());
        } catch(Exception $e) {
            // Logging $e
            Log::error($e);
            
            // return empty array
            return json_decode("[]");
        }
    }

    public function get_product_from_category($category_id) {
        try {
            $response = $this->guzzle->get("products?category={$category_id}");
            return json_decode($response->getBody());
        } catch(Exception $e) {
            // Loggging $e
            // return empty array
            return json_decode("[]");
        }
    }

    public function get_products_by_param_ids(string $param, array $ids) {
        $str_ids = implode(",", $ids);
        try {
            $response = $this->guzzle->get("{$param}?include={$str_ids}");
            return json_decode($response->getBody());
        } catch(Exception $e) {
            // Loggging $e
            Log::error($e);

            // return empty array
            return json_decode("[]");
        }
    }
}