<?php
namespace App\Repository;

interface UserCommission {
    public function total_commission();
    public function commission($order_id);
} 