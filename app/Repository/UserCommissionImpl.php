<?php
namespace App\Repository;

use App\Order;
use App\Commission;
use App\Model\CommissionOrder; 
use App\Repository\UserCommision;
use App\Repository\ProductVendorRepo;

class UserCommissionImpl implements UserCommission {
    private $comm_order;
    private $list_comm_order;
    private $user_id;
    private $product_vendor;
    public $percent_commission = 0.05;

    // $dis is Discount from original price for commission
    private $disc;

    public function __construct($user_id, ProductVendorRepo $product_vendor) {
        $this->user_id = $user_id;
        $this->product_vendor = $product_vendor;
        $this->calculate_commision();
    }

    /**
     * calculate_commision for calculate the commision from actual prices
     * and store it in arrays of order
     */
    private function calculate_commision() {
        // get user orders history
        $orders = Order::where(['user_id' => $this->user_id, 'commission_id' => NULL])->get();
        
        // get orders detail from vendor
        $order_ids = [];
        foreach ($orders as $key => $value) {
            $order_ids[$key] = $value->order_id;
        }

        $this->list_comm_order = [];

        $orders = $this->product_vendor->get_products_by_param_ids("orders", $order_ids);
        foreach ($orders as $key => $value) {
            // push only order that has been paid
            if($value->status == "processing" || $value->status == "completed") {
                $this->list_comm_order[$value->id] = new CommissionOrder($value->id, $value->total * $this->percent_commission);
            }
        }
    }

    public function get_list_comm_order() {
        return $this->list_comm_order;
    }

    public function total_commission() {
        $total = 0;
        foreach ($this->list_comm_order as $key => $order) {
            $total = $total + $order->get_commission();
        }

        return $total;
    }

    public function commission($order_id) {
        return $list_comm_order[$order_id];
    }
}