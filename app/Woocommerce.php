<?php 

namespace App;

use Automattic\WooCommerce\Client;
/**
 * 
 */
class Woocommerce 
{
	public $woocommerce;

	function __construct()
	{
		$this->woocommerce = new Client(
			getenv("VENDOR_URL"),
			getenv("WOO_CK"),
			getenv("WOO_CS"),
			[
				'wp_api' => true,
				'version' => 'wc/v2',
				'timeout' => 1000,
				'query_string_auth' => true,
			]
		);
	}
}
