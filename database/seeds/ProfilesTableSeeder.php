<?php

use Illuminate\Database\Seeder;
use App\Profile;

class ProfilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $profile = new Profile;
        $profile->name = 'admin';
        $profile->contact = '1234567890';
        $profile->address = 'xxx';
        $profile->bank_account = '1234567890';
        $profile->bank_type = 'BNI';
        $profile->user_id = 1;
        $profile->save();
    }
}
