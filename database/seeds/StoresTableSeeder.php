<?php

use Illuminate\Database\Seeder;
use App\Store;

class StoresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $store = new Store;
        $store->name = 'Admin';
        $store->logo = 'default-logo.png';
        $store->url = 'Admin';
        $store->user_id = 1;
        $store->save();
    }
}
