@extends('layouts.apptemp')

@section('content')

<div class="container">

    <form class="form-horizontal" method="POST" action="{{ route('store-update') }}" enctype="multipart/form-data">
        {{ csrf_field() }}

        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
            <label for="name" class="col-md-4 control-label">Name</label>

            <div class="col-md-6">
                <input id="name" type="text" class="form-control" name="name" value="{{ $store->name }}" disabled>

                @if ($errors->has('name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('url') ? ' has-error' : '' }}">
            <label for="url" class="col-md-4 control-label">URL</label>

            <div class="col-md-6 row">
                <!-- <textarea id="url" class="form-control" name="url" required></textarea> -->
                <div class="col-md-10 input-group mb-3">
                    <input id="url" type="text" class="form-control" name="url" value="{{ $store->url }}" disabled>                  
                </div>
                <div class="col-md-2">
                    <a href="{{ 'http://'.$store->url }}" rel="noopener noreferrer" target="_blank" class="btn btn-primary bm-bg-sec bm-border-sec">kunjungi</a>
                </div>

                @if ($errors->has('url'))
                    <span class="help-block">
                        <strong>{{ $errors->first('url') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('sosmed_fb') ? ' has-error' : '' }}">
            <label for="sosmed_fb" class="col-md-4 control-label">FB</label>

            <div class="col-md-6">
                <!-- <textarea id="sosmed_fb" class="form-control" name="sosmed_fb" required></textarea> -->
                <div class="input-group mb-3">
                    <input id="sosmed_fb" type="text" class="form-control" name="sosmed_fb" value="{{ $store->sosmed_fb }}">
                </div>

                @if ($errors->has('sosmed_fb'))
                    <span class="help-block">
                        <strong>{{ $errors->first('sosmed_fb') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('sosmed_instagram') ? ' has-error' : '' }}">
            <label for="sosmed_instagram" class="col-md-4 control-label">Instagram</label>

            <div class="col-md-6">
                <!-- <textarea id="sosmed_instagram" class="form-control" name="sosmed_instagram" required></textarea> -->
                <div class="input-group mb-3">
                    <input id="sosmed_instagram" type="text" class="form-control" name="sosmed_instagram" value="{{ $store->sosmed_instagram }}">
                </div>

                @if ($errors->has('sosmed_instagram'))
                    <span class="help-block">
                        <strong>{{ $errors->first('sosmed_instagram') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('sosmed_line') ? ' has-error' : '' }}">
            <label for="sosmed_line" class="col-md-4 control-label">Line</label>

            <div class="col-md-6">
                <!-- <textarea id="sosmed_line" class="form-control" name="sosmed_line" required></textarea> -->
                <div class="input-group mb-3">
                    <input id="sosmed_line" type="text" class="form-control" name="sosmed_line" value="{{ $store->sosmed_line }}">
                </div>

                @if ($errors->has('sosmed_line'))
                    <span class="help-block">
                        <strong>{{ $errors->first('sosmed_line') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('sosmed_wa') ? ' has-error' : '' }}">
            <label for="sosmed_wa" class="col-md-4 control-label">Whatsapp</label>

            <div class="col-md-6">
                <!-- <textarea id="sosmed_wa" class="form-control" name="sosmed_wa" required></textarea> -->
                <div class="input-group mb-3">
                    <input id="sosmed_wa" type="text" class="form-control" name="sosmed_wa" value="{{ $store->sosmed_wa }}">
                </div>

                @if ($errors->has('sosmed_wa'))
                    <span class="help-block">
                        <strong>{{ $errors->first('sosmed_wa') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('logo') ? ' has-error' : '' }}">
            <label for="logo" class="col-md-4 control-label">Logo</label>

            <div class="col-md-6">
                <img src="{{ url('').'/storage/'.$store->logo }}" id="output_image" width="90px"/>
                <br>
                <input type="file" name="logo" onchange="preview_image(event)">

                @if ($errors->has('logo'))
                    <span class="help-block">
                        <strong>{{ $errors->first('logo') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-8 col-md-offset-4">
                <button type="submit" class="btn btn-primary bm-bg-sec bm-border-sec">
                    Simpan
                </button>
            </div>
        </div>
    </form>
</div>

<script type='text/javascript'>
  function preview_image(event) 
  {
    var reader = new FileReader();
    reader.onload = function() {
      var output = document.getElementById('output_image');
      output.src = reader.result;
    }
    reader.readAsDataURL(event.target.files[0]);
  }
</script>



@endsection