@extends('layouts.applogin')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    Anda belum melakukan verifikasi email, silahkan melakukan verifikasi melalui email yang telah anda terima!!!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
