@extends('layouts.apptempadmin')

@section('content')

    <div class="container">

        <form class="form-horizontal" method="POST" action="{{ url('admin/promotions/add') }}" enctype="multipart/form-data">
            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                <label for="title" class="col-md-4 control-label">Title</label>

                <div class="col-md-6">
                    <input id="title" type="text" class="form-control" name="title" required>

                    @if ($errors->has('title'))
                        <span class="help-block">
                            <strong>{{ $errors->first('title') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                <label for="type" class="col-md-4 control-label">Type</label>

                <div class="col-md-6">
                    <select class="custom-select" name="type" required>
                        <option value="0" selected>Image</option>
                    </select>

                    @if ($errors->has('type'))
                        <span class="help-block">
                            <strong>{{ $errors->first('type') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('logo') ? ' has-error' : '' }}">
            <label for="path" class="col-md-4 control-label">File</label>

            <div class="col-md-6">
                <img src="#" id="output_image" width="90px"/>
                <br>
                <input type="file" name="path" onchange="preview_image(event)" required>

                @if ($errors->has('path'))
                    <span class="help-block">
                        <strong>{{ $errors->first('path') }}</strong>
                    </span>
                @endif
            </div>
        </div>

            <div class="form-group">
                <div class="col-md-8 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        Simpan
                    </button>
                </div>
            </div>
        </form>
    </div>

    <script type='text/javascript'>
      function preview_image(event) 
      {
        var reader = new FileReader();
        reader.onload = function() {
          var output = document.getElementById('output_image');
          output.src = reader.result;
        }
        reader.readAsDataURL(event.target.files[0]);
      }
    </script>

@endsection