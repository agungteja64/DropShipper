@extends('layouts.applogin')


@section('content')

<section class="container">
    <h3>Cara Pembayaran</h3>
    <ol>
        <li class="pb-2">
            Transfer via ATM/Bank ke <br>
            10 111111 2222 <br>
            a.n. Owner Bermitra
        </li>
        <li class="pb-2">
            Konfirmasi pembayaran ke <a href="/konfirmasi-pembayaran">link</a>
        </li>
        <li class="pb-2">
            Silahkan menunggu, tim kami akan memroses pendaftaran anda.
        </li>
    </ol>
</section>
@endsection
