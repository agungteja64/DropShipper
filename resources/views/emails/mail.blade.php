<p>Terima kasih telah mendaftar di Bermitra.id.</p>
<p>Silahkan melakukan konfirmasi email dengan mengklik <a href="{{ url('verify',$token) }}">link</a>.</p>

<p>Dan melakukan pembayaran registrasi sebesar: <br> 
<b> Rp600.000,- </b> <br>
ke rekening berikut:</p>
<p>00 0000 0000 0000</p>
<p>a.n. Nama_Pemilik_Rekening</p>

<p>Kirimkan bukti transfer dengan membalas email ini</p>