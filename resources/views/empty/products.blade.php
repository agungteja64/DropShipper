@extends('layouts.apptemp')

@section('content')

<div class="container">
    <form class="form-horizontal" method="POST" action="{{ route('products-update') }}">
        {{ csrf_field() }}
        
        <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
            <h3>Produk</h3>
            <br>
            <br>
            <div class="form-group">
                <h4 class="text-center text-muted"><em>Produk kosong</em></h4>
            </div>
        </div>
    </form>
</div>

<script type='text/javascript'>
      function preview_image(event) 
      {
        var reader = new FileReader();
        reader.onload = function() {
          var output = document.getElementById('output_image');
          output.src = reader.result;
        }
        reader.readAsDataURL(event.target.files[0]);
      }
    </script>



@endsection