@extends('layouts.apptemp')

@section('content')
<div class="container w-80">
    <h2>Komisi</h2>

    <div class="row">
        <section class="col-md-3">
            <div class="bg-info px-2 py-2 pl-3 text-light">
                <h5>Total</h5>
                <span class="fa-2x font-weight-bold"><small>Rp</small>{{ number_format($total_commission, 2, ",", ".") }}</span>
            </div>    
        </section>        
    </div>        
    <section class="mt-3 w-80">
        <form action="{{ url('komisi/create') }}" method="POST">
            @csrf
            <table class="table-striped w-100">
                <thead>
                    <tr class="py-2">
                        <th scope="col" class="px-3 py-2 w-10">No. Order</th>
                        <th scope="col" class="px-3 py-2 w-90">Komisi</th>
                    </tr>
                </thead>
        {{-- <a href="{{ url('commission/download') }}" class="btn btn-primary">Download Commission</a> --}}
            <tbody>
                @foreach($list_order as $order)
                    <tr class="py-2">
                        <td class="px-3 py-2 w-10">{{ $order->get_order_id() }}</td>
                        <td class="px-3 py-2 w-90"><small>Rp</small>{{ number_format($order->get_commission(), 2, ",", ".") }}</td>
                        <input type="hidden" name="orders[]" value="{{ $order->get_order_id() }}">
                    </tr>
                @endforeach
            </tbody>
        </table>
        <input type="hidden" name="commission" value="{{ $total_commission * $percent_commission }}">
        @if(count($list_order) == 0)
            <!-- disabled button -->
            <input type="submit" class="mt-2 btn btn-primary bm-bg-sec bm-border-sec" name="Get Commission" value="Get Commission" disabled>
        @else
            <input type="submit" class="mt-2 btn btn-primary bm-bg-sec bm-border-sec" name="Get Commission" value="Get Commission">
        @endif
    </form>
    </section>
</div>

@endsection