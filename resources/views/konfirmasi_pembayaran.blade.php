@extends('layouts.applogin')


@section('content')

<section class="container">
    <h4>Konfirmasi Pembayaran</h4>

    <form action="/konfirmasi-pembayaran" method="post">
        <label for="email"><small>Email</small></label> <br>
        <input class="w-100" type="text" name="email" id=""> <br>

        <label class="pt-3" for="bukti-transfer"><small>Bukti Transfer</small></label> <br>
        <input class="w-100" type="file" name="butki-transfer" id=""> <br>

        <button type="submit" class="btn btn-primary mt-5 w-100">Kirim</button>
    </form>

</section>
@endsection
