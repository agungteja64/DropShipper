<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Bermitra</title>

    <!-- Bootstrap core CSS-->
    <link href="{{ asset('bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    
    <!-- Custom fonts for this template-->
    {{-- <link href="{{ asset('font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css"> --}}
    {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> --}}
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

    <!-- custom css for landing page -->
    <link rel="stylesheet" href="{{ asset('css/landing_page.css') }}">
</head>
<body>
    @if (Route::has('login'))
        <header class="nav navbar bm-nav">
            <nav class="container mt-2">
                <a href="/" class="nav-item bm-nav-item logo">
                    Bermitra</a>
                <ul class="nav justify-content-end">
                    @auth
                        <li class="nav-item">
                            <a href="{{ route('store-detail') }}"
                                class="nav-item bm-nav-item">
                                HOME
                            </a>
                        </li>
                    @else
                        <li class="nav-item">
                            <a href="{{ route('register') }}" 
                                class="nav-item bm-nav-item">
                                DAFTAR
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('login') }}"
                                class="nav-item bm-nav-item">
                                LOGIN
                            </a>
                        </li>
                        <!-- <li class="nav-item bm-nav-item">
                            <a href=""
                                class="nav-item">
                                HUBUNGI KAMI
                            </a>
                        </li>
                        <li class="nav-item bm-nav-item">
                            <a href=""
                                class="nav-item">
                                TENTANG
                            </a>
                        </li> -->
                    @endauth
                </ul>                
            </nav>
        </header>
    @endif

    <main class="container mt-4 landing-people">
        <h1 class="col-12 text-display bm-color-prim">
            Semua Bisa <br> 
            Berbisnis Online </h1>

        <section class="col-md-8 ">
            <h4 class="text-sub bm-color-dark">
                Bersama Bermitra</h4>
    
            <p class="text-body">
                ALASAN MENGAPA ANDA PERLU BERMITRA DENGAN KAMI:</p>
            
            <!-- 1 -->
            <div class="col-12 row text-reason">
                <img src="{{ asset('icons/custom/money.png') }}" alt="money" 
                    class="col-md-4 icon"/>

                <div class="col-md-8 text-body">
                    <p class="desc-head">PRODUK YANG DIJUAL TERBUKTI LARIS <br>
                        <span class="desc">Tidak perlu ragu lagi untuk bermitra kepada kami. 
                        Barang yang disediakan sudah terbukti laris di pasaran.
                        Dapatkan untungnya sekarang juga!</span>
                    </p>
                </div>
            </div>

            <!-- 2 -->
            <div class="col-12 row text-reason">
                <img src="{{ asset('icons/custom/cart.png') }}" alt="cart" 
                    class="col-md-4 icon"/>

                <div class="col-md-8 text-body">
                    <p class="desc-head">UNTUNG TANPA HARUS BINGUNG<br>
                        <span class="desc">Anda tidak perlu lagi memikirkan ketersediaan barang,
                            masalah produksi kami yang atur.
                        </span>
                    </p>
                </div>
            </div>

            <div class="col-12 row text-reason">
                <img src="{{ asset('icons/custom/notes.png') }}" alt="notes" 
                    class="col-md-4 icon"/>

                <div class="col-md-8 text-body">
                    <p class="desc-head">MUDAH DIGUNAKAN <br>
                        <span class="desc">Gapterk bukan lagi menjadi mimpi buruk untuk maju dalam 
                            kemajuan industri ekonomi. Aplikasi Bermitra.id kami rancang dengan kemudahan
                            akses dan ramah terhadap pengguna.
                        </span>
                    </p>
                </div>
            </div>

            <div class="col-12 row text-reason">
                <img src="{{ asset('icons/custom/hand-shake.png') }}" alt="notes" 
                    class="col-md-4 icon"/>

                <div class="col-md-8 text-body">
                    <p class="desc-head">MAJU BERSAMA <br>
                        <span class="desc">Bermitra.id jadikan Anda seorang pebisnis online dengan menyediakan pelatihan singkat 
                            bagi pengguna baru. Tidak perlu latihan lama-lama. Tinggal daftar, belajar sebentar, dan 
                            raup untung sebesar-besarnya
                        </span>
                    </p>
                </div>
            </div>

        </section>
    </main>

    <footer class="text-center mt-5 bg-dark pt-4 pb-4">
        <!-- Copy right -->
        <small class="d-block text-muted">
            &copy; Bermitra 2018
        </small>
    </footer>

</body>
</html>
