@extends('layouts.apptempadmin')

@section('content')

<div class="container-fluid">
    <h3>Users</h3>

    <table class="table">
      <thead class="">
        <tr>
          <th scope="col">#</th>
          <th scope="col">User Name</th>
          <th scope="col">Store Name</th>
          <th scope="col">Total Komisi</th>
          <th scope="col">Status Commission</th>
          <th scope="col">Option</th>
        </tr>
      </thead>
      <tbody>
        <?php $i = 1; ?>
        @foreach($commissions as $commission)
            <tr>
              <th scope="row">{{ $i }}</th>
              <td>{{ $commission->orders[0]->user->profile->name }}</td>
              <td>{{ $commission->orders[0]->user->store->name }}</td>
              <td><small>Rp</small>{{ number_format($commission->commission, 2, ",", ".") }}</td>
              @if($commission->status == 1)
                <td>Sudah diberi komisi</td>
                <td></td>
              @elseif($commission->status == 0)
                <td>Belum diberi komisi</td>
                <form method="POST" action="{{ url('admin/commissions') }}">
                    @csrf
                    <input type="hidden" name="id" value="{{ $commission->id }}">
                    <td><input type="submit" name="submit" value="Beri komisi"></td>
                </form>
              @else
                <td>Active</td>
                <td></td>
              @endif
            </tr>
            <?php $i++; ?>
        @endforeach
      </tbody>
    </table>

    <br>
    <a href="{{ url('admin/commissions/download') }}" class="btn btn-primary bm-bg-sec bm-border-sec">Download Komisi</a>
    
</div>


@endsection