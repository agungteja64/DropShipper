@extends('layouts.apptempadmin')

@section('content')

<div class="container-fluid">
    <h3>Promotions</h3>
    <a href="{{ url('admin/promotions/add') }}" class="btn btn-primary bm-bg-sec bm-border-sec">Add Promotion</a>
    <br>
    <br>
    <table class="table">
      <thead class="">
        <tr>
          <th scope="col">#</th>
          <th scope="col">Image</th>
          <th scope="col">Title</th>
          <th scope="col"></th>
        </tr>
      </thead>
      <tbody>
        <?php $i = 1; ?>
        @foreach($promotions as $promotion)
            <tr>
              <th scope="row">{{ $i }}</th>
              <td><a href="{{ url('admin/promotions/'.substr($promotion->path, strpos($promotion->path, '/') + 1).'/download') }}" target="_blank"><img src="{{ url('').'/storage/'.$promotion->path }}" width="90px"></a></td>
              <td><a href="{{ url('admin/promotions/'.substr($promotion->path, strpos($promotion->path, '/') + 1).'/download') }}" target="_blank">{{ $promotion->title }}</a></td>
              <td><a href="/admin/promotions/{{ $promotion->id }}/delete"><i class="fa fa-trash"></i></a></td>
            </tr>
            <?php $i++; ?>
        @endforeach
      </tbody>
    </table>
</div>


@endsection