@extends('layouts.apptempadmin')

@section('content')

<div class="container-fluid">
    <h3>Tutorials</h3>
    <a href="{{ url('admin/tutorials/add') }}" class="btn btn-primary bm-bg-sec bm-border-sec">Add Video</a>
    <br>
    <br>
    <div class="form-group">
      @foreach($videos as $video)
        <div class="col-md-4 d-flex flex-column align-items-center">
            <iframe height="150px" src="{{ $video->link }}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

            <div>
                <p>{{ $video->title }}</p>  
            </div>
        </div>
      @endforeach
    </div>
</div>


@endsection