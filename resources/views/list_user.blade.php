@extends('layouts.apptempadmin')

@section('content')

<div class="container-fluid">
    <h3>Users</h3>

    <table class="table table-responsive">
      <thead class="">
        <tr>
          <th scope="col">#</th>
          <th scope="col">Username</th>
          <th scope="col">Name</th>
          <th scope="col">Bank Account</th>
          <th scope="col">Contact</th>
          <th scope="col">Store Name</th>
          <th scope="col">Status</th>
          <th scope="col">Option</th>
        </tr>
      </thead>
      <tbody>
        <?php $i = 1; ?>
        @foreach($users as $user)
            <tr>
              <th scope="row">{{ $i }}</th>
              <td>{{ $user->username }}</td>
              <td>{{ $user->profile->name }}</td>
              <td>{{ $user->profile->bank_account }}</td>
              <td>{{ $user->profile->contact }}</td>
              <td>{{ $user->store->name }}</td>
              @if($user->status == 0)
                <td>Belum Confirm Email</td>
                <td></td>
              @elseif($user->status == 1)
                <td>Belum Bayar Billing</td>
                <form method="POST" action="{{ url('admin/users') }}">
                    @csrf
                    <input type="hidden" name="user_id" value="{{ $user->id }}">
                    <td><input type="submit" name="submit" value="Activate"></td>
                </form>
              @else
                <td>Active</td>
                <td></td>
              @endif
            </tr>
            <?php $i++; ?>
        @endforeach
      </tbody>
    </table>
    
</div>


@endsection