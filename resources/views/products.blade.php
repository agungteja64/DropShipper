@extends('layouts.apptemp')

@section('content')

<div class="container">
    <form class="form-horizontal" method="POST" action="{{ route('products-update') }}">
        {{ csrf_field() }}
        
        <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
            <table class="table">
                <thead class="">
                  <tr>
                    <th scope="col">Produk</th>
                    <th scope="col">Harga</th>
                    <th scope="col">Brand</th>
                    <th scope="col">Pilih</th> 
                  </tr>
                </thead>
                <tbody>
                    @foreach($subscribed as $sub)
                        <tr>
                            <td>
                                <img 
                                    style="width: 80px" 
                                    src="{{ $sub->images[0]->src }}" 
                                    alt="{{ $sub->images[0]->alt }}" />  

                                {{ $sub->name }}
                            </td>
                            <td><small>Rp</small> {{ number_format($sub->price, 2, ",", ".") }},-</td>
                            <td>{{ $sub->categories[0]->name }}</td>
                            <td><input type="checkbox" name="category[]" value="{{ $sub->id }}" checked></td> 
                        </tr>
                    @endforeach
                    @foreach($unsubscribed as $unsub)
                        <tr>
                          <td>
                            <img 
                                style="width: 80px;" 
                                src="{{ $unsub->images[0]->src }}" 
                                alt="{{ $unsub->images[0]->alt }}" />  
                                
                            {{ $unsub->name }}
                          </td>
                          <td><small>Rp</small> {{ number_format($unsub->price, 2, ",", ".") }},-</td>
                          <td>{{ $unsub->categories[0]->name }}</td>
                          <td><input type="checkbox" name="category[]" value="{{ $unsub->id }}"></td> 
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

        <div class="form-group">
            <div class="col-md-8 col-md-offset-4">
                <button type="submit" class="btn btn-primary bm-bg-sec bm-border-sec">
                    Simpan
                </button>
            </div>
        </div>
    </form>
</div>

<script type='text/javascript'>
      function preview_image(event) 
      {
        var reader = new FileReader();
        reader.onload = function() {
          var output = document.getElementById('output_image');
          output.src = reader.result;
        }
        reader.readAsDataURL(event.target.files[0]);
      }
    </script>



@endsection