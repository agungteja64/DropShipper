<table>
    <thead>
    <tr>
        <th>Name</th>
        <th>Tgl Request</th>
        <th>Jumlah</th>
        <th>Rekening</th>
        <th>Bank Tujuan</th>
    </tr>
    </thead>
    <tbody>
    @foreach($commissions as $commission)
        <tr>
            <td>{{ $commission->orders[0]->user->profile->name }}</td>
            <td>{{ $commission->created_at }}</td>
            <td>{{ $commission->commission }}</td>
            <td>{{ $commission->orders[0]->user->profile->bank_account }}</td>
            <td>{{ $commission->orders[0]->user->profile->bank_type }}</td>
        </tr>
    @endforeach
    </tbody>
</table>