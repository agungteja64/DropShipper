@extends('layouts.apptemp')

@section('content')

<div class="container-fluid">
    <h3>Promotions</h3>
    <br>
    <br>
    <div class="form-group">
        @if(count($promotions) <= 0)
            <h4 class="text-center text-muted"><em>Tidak ada konten marketing</em></h4>
        @else
            @foreach($promotions as $promotion)
                <div class="col-md-4 d-flex flex-column align-items-center">
                    <!-- <iframe height="150px" src="{{ $promotion->link }}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe> -->
                    <img src="{{ url('').'/storage/'.$promotion->path }}" alt="{{ $promotion->title }}" 
                        class="img-fluid img-thumbnail" />
                    <div>
                        <p>{{ $promotion->id }}</p>  
                    </div>
                </div>
            @endforeach
        @endif
    </div>
</div>

@endsection