@extends('layouts.apptemp')

@section('content')

<div class="container-fluid">
    <h3>Tutorials</h3>
    <br>
    <br>
    <div class="form-group">
    @if(count($videos) <= 0)
        <h4 class="text-center text-muted"><em>Tidak ada video tutorial</em></h4>
    @else
        @foreach($videos as $video)
            <div class="col-md-4 d-flex flex-column align-items-center">
                <iframe height="150px" src="{{ $video->link }}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

                <div>
                    <p>{{ $video->title }}</p>  
                </div>
            </div>
        @endforeach
    @endif
    </div>
</div>


@endsection