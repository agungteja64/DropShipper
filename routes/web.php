<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::get('/', function () {
    return view('landing_page');
});

Route::get('/warning', function () {
    return view('home');
})->name('warning');

Route::get('/restrict', function () {
    return view('not_admin');
})->name('not-admin');

Route::get('/forceLogout', function() {
    Auth::logout();
    return redirect('/');
})->name('forceLogout');

Route::get('/a', 'HomeController@aaa');
// Route::get('/send_mail', 'HomeController@send_mail');
Route::get('billing', 'HomeController@billing');
Route::get('email', 'HomeController@email');

Route::get('/cara-pembayaran', function() {
    return view('cara_pembayaran');
})->name('cara-pembayaran');

Route::get('/konfirmasi-pembayaran', function() {
    return view('konfirmasi_pembayaran');
})->name('konfirmasi-pembayaran');


Route::get('activeyouracc', 'Auth\AuthController@userNotActive')->name('user-not-active')->middleware('auth');

Route::get('/verify/{token}', 'HomeController@verify_email');

Route::get('/store', 'StoreController@detail')->name('store-detail')->middleware(['auth', 'isAdmin', 'isActive']);
Route::post('/store', 'StoreController@update')->name('store-update')->middleware(['auth', 'isAdmin', 'isActive']);

Route::get('/profile', 'ProfileController@detail')->name('profile-detail')->middleware(['auth', 'isAdmin', 'isActive']);
Route::post('/profile', 'ProfileController@update')->name('profile-update')->middleware(['auth', 'isAdmin', 'isActive']);

Route::get('/products', 'StoreController@vendor_list')->name('products-list')->middleware(['auth', 'isAdmin', 'isActive']);
Route::post('/products', 'StoreController@vendor_update')->name('products-update')->middleware(['auth', 'isAdmin', 'isActive']);
Route::get('/products/{cat_id}', 'StoreController@vendor_by_category')->name('products-list-by-category')->middleware(['auth', 'isAdmin', 'isActive']);

Route::get('/orders', 'StoreController@order_list')->name('order-list')->middleware(['auth', 'isAdmin', 'isActive']);

//Route::get('/commissions', 'CommissionController@getCommissionStatusZero')->name('comission-zero')->middleware(['auth', 'isAdmin', 'isActive']);

Route::get('/komisi', 'StoreController@get_comission')->name('get-commission')->middleware(['auth', 'isAdmin', 'isActive']);
Route::post('/komisi/create', 'CommissionController@createCommission')->middleware(['auth', 'isAdmin', 'isActive']);

//Route::get('/komisi/download', )

Route::get('/riwayat_komisi', 'StoreController@history_comsission')->name('history-commission')->middleware(['auth', 'isAdmin', 'isActive']);

Route::get('/tutorials', 'StoreController@show_marketing_tuts')->name('show-marketing-tuts')->middleware(['auth', 'isAdmin', 'isActive']);
Route::get('/konten-marketing', 'StoreController@show_marketing_content')->name('show-marketing-content')->middleware(['auth', 'isAdmin', 'isActive']);


//admin
Route::prefix('admin')->group(function () {
    Route::get('users', 'AdminController@list_user')->name('admin-users')->middleware(['auth', 'isNotAdmin']);
    Route::post('users', 'AdminController@activate_user')->middleware(['auth', 'isNotAdmin']);

    Route::get('tutorials', 'TutorialController@list_tutorial')->name('admin-tutorials')->middleware(['auth', 'isNotAdmin']);
    Route::get('tutorials/add', 'TutorialController@show_add_tutorials')->middleware(['auth', 'isNotAdmin']);
    Route::post('tutorials/add', 'TutorialController@add_tutorials')->middleware(['auth', 'isNotAdmin']);

    Route::get('promotions', 'PromotionController@list_promotion')->name('admin-promotions')->middleware(['auth', 'isNotAdmin']);
    Route::get('promotions/add', 'PromotionController@show_add_promotions')->middleware(['auth', 'isNotAdmin']);
    Route::post('promotions/add', 'PromotionController@add_promotions')->middleware(['auth', 'isNotAdmin']);
    Route::get('promotions/{path}/download', 'PromotionController@download_promotion')->middleware(['auth', 'isNotAdmin']);
    Route::get('promotions/{id}/delete', 'PromotionController@delete_promotion')->middleware(['auth', 'isNotAdmin']);

    //Route::get('commissions', 'CommissionController@show_all_orders')->name('admin-commissions')->middleware(['auth', 'isNotAdmin']);
    Route::get('commissions', 'CommissionController@showCommissions')->name('admin-commissions')->middleware(['auth', 'isNotAdmin']);
    Route::post('commissions', 'CommissionController@change_status_commissions')->middleware(['auth', 'isNotAdmin']);
    Route::get('commissions/download', 'CommissionController@downloadRecapCommissions')->name('admin-commissions-download')->middleware(['auth', 'isNotAdmin']);
});

use Automattic\WooCommerce\Client;
use Automattic\WooCommerce\HttpClient\HttpClientException;

Route::get('/wcc', function () {

	$woocommerce = new Client(
	    'https://codeandblue.com/main_dropship', 
	    'ck_e2a5069fd4d20488637f1f4bd48bef5c1986b693', 
        'cs_e89ef23e2ce5ffbdeed449f3bba1a69ebbbf69ab',
        [
            'wp_api' => true,
            'version' => 'wc/v2',
            'timeout' => 1000,
        ]
	);

    dd($woocommerce->get('orders'));

    try {
        // Array of response results.
        $results = $woocommerce->get('customers');
        // Example: ['customers' => [[ 'id' => 8, 'created_at' => '2015-05-06T17:43:51Z', 'email' => ...

        // Last request data.
        $lastRequest = $woocommerce->http->getRequest();
        $lastRequest->getUrl(); // Requested URL (string).
        $lastRequest->getMethod(); // Request method (string).
        $lastRequest->getParameters(); // Request parameters (array).
        $lastRequest->getHeaders(); // Request headers (array).
        $lastRequest->getBody(); // Request body (JSON).

        // Last response data.
        $lastResponse = $woocommerce->http->getResponse();
        $lastResponse->getCode(); // Response code (int).
        $lastResponse->getHeaders(); // Response headers (array).
        $lastResponse->getBody(); // Response body (JSON).

    } catch (HttpClientException $e) {
        $e->getMessage(); // Error message.
        $e->getRequest(); // Last request data.
        $e->getResponse(); // Last response data.
        dd($e->getMessage());
    }
    
});


Route::get('/home', 'HomeController@index')->name('home');


Route::get('/testing', function() {
    dd(Request::getHost());
});